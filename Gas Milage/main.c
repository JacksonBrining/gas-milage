#include <stdio.h>
#include <stdlib.h>

// Enter the gallons used (-1 to end): 12.8
// Enter the miles driven: 287
// The miles/gallon for this tank was 22.421875

// Enter the gallons used (-1 to end): 10.3
// Enter the miles driven: 200
// The miles/gallon for this tank was 19.417475

// Enter the gallons used (-1 to end): 5
// Enter the miles driven: 120
// The miles/gallon for this tank was 24.000000

// Enter the gallons used (-1 to end): -1
// The overall average miles/gallon was 21.601423

int main( void )
{

    unsigned int counter;
    int gallons;
    int miles;
    int total;
    int totalM;

    float average;

    total = 0;
    totalM = 0;
    counter = 0;

    printf("%s", "Enter Gallons used, type -1 to calculate: ");
    scanf("%d", &gallons);

    while (gallons != -1) {
        total = total + gallons;
        counter = counter + 1;

        printf("%s", "Enter Gallons used, type -1 to calculate: ");
        scanf("%d", &gallons);

    }

    printf("%s", "Enter the miles driven: type -1 to calculate: ");
    scanf("%d", &miles);

    while (miles != -1) {
        totalM = totalM + miles;
        counter = counter + 1;

        printf("%s", "Enter the miles driven, type -1 to calculate: ");
        scanf("%d", &miles);

    }

    if(counter != 0){

        average = (float) total / (float) totalM + counter ;

        printf("Average MPG for the trips were %.2f\n", average);
    }

    else {
        puts("Not enough information was entered");
    }

}
